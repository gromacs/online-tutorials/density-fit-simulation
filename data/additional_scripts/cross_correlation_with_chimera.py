from chimera import runCommand

import os


def get_cc(path_to_output='output folder path', density_name='CGRPR_active_state_map_rotated.mrc'):
        path_to_density = path_to_output+'/'+density_name
        print(path_to_density)
        models = []
        cc_list = []
        runCommand('''open {path_to_density}\n'''.format(path_to_density = path_to_density))

        file_list = os.listdir('{path_to_output}/out/'.format(path_to_output = path_to_output))
        file_list_sorted = sorted(file_list, key = lambda x: int(x[6:-4]))
        for i in file_list_sorted:
                if i[-3:]=='pdb':
                        models.append(i[:-4])
                        runCommand('''open {path_to_output}/out/{i}\n'''.format(path_to_output = path_to_output, i = i))
                        runCommand('molmap #1 3')
                        runCommand('fitmap #1.1 #0 shift false rotate false metric correlation')
                        runCommand('close #1')
                        runCommand('close #1.1')

get_cc()