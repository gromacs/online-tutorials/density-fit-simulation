{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hide_input": true
   },
   "outputs": [],
   "source": [
    "%run src/init_notebooks.py\n",
    "#check_notebook()\n",
    "hide_toggle()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Density-fit simulation #"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Authors :  Tatiana Shugaeva, Nandan Haloi, John Cowgill and Alessandra Villa \n",
    "\n",
    "Goal : learn step-by-step how to run a density-fit simulation using GROMACS\n",
    "\n",
    "Time : 60 minutes\n",
    "\n",
    "Software : GROMACS 2024, python modules: nglviewer, MDAnalysis, pandas, matplotlib.\n",
    "\n",
    "Source: [GROMACS tutorials @gromacs.org](https://tutorials.gromacs.org/) \n",
    "\n",
    "version : release [doi:10.5281/zenodo.13938906]()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction to density fitting simulations ##\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Recent advances in single-particle cryoelectron microscopy (cryo-EM) have enabled us to obtain near-atomic resolution structures of challenging protein targets including large protein complexes and membrane proteins. However, in many cases, one often ends up in low-resolution densities that are not sufficient to confidently build models. This mainly occurs due to the inherent dynamics of the protein or bound-ligands such as lipids, small molecules; and the averaging of each EM micrograph during 3-dimensional density construction leads to low resolution averaged map for these dynamic parts. \n",
    "\n",
    "Several molecular dynamics (MD) simulations based methods have been developed to refine all-atom models to EM maps, e.g. correlation-driven molecular dynamics (CDMD) simulations that uses EM density maps to add potential to guide an atomic model to the target [Igaev et al. eLife 2019;8:e43542], molecular dynamic flexible fitting (MDFF) that applies forces proportional to the gradient of the EM density map [Singharoy et al. eLife 2016;5:e16105]. \n",
    "\n",
    "Here, we use the [GROMACS](http:\\\\www.gromacs.org) to perform density-fit (or density-guided) simulations. In density-guided simulations, additional forces are applied to atoms that depend on the gradient of similarity between a simulated density and a reference density. By applying these forces protein structures can be \"fitted\" to densities coming from, e.g., cryo electron-microscopy (see  details in the [GROMACS manual](https://manual.gromacs.org/current/reference-manual/special/density-guided-simulation.html). The implemented approach extends the ones described in [Orzechowski et al.](https://doi.org/10.1529/biophysj.108.139451) and [Igaev et al.}](https://doi.org/10.7554/eLife.43542)\n",
    "\n",
    "More on the density guided approach implemented in GROMACS and a recently developed tool within GROMACS that can fit structures via Bayes' approach can be found in [Blau et al. bioRxiv 2022](https://doi.org/10.1101/2022.09.30.510249). \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preparations to run this notebook ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First of all, we need to install a tool that allow us to automatically align density to the system coordinates: [rigidbodyfit](https://pypi.org/project/rigidbodyfit/). We will use this tool in the pre-processing part. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! pip install rigidbodyfit"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then we import the follow post-processing tools from MDAnalysis "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import warnings\n",
    "# suppress some MDAnalysis warnings about PSF files\n",
    "warnings.warn('ignore')\n",
    "import nglview as ng\n",
    "import MDAnalysis as mda\n",
    "from MDAnalysis.analysis import rms\n",
    "from MDAnalysis.analysis import align\n",
    "from MDAnalysis.analysis.rms import RMSF\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we move to the working directory `data`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Change to the data directory\n",
    "# Note that executing this command twice will result in an error you can ignore\n",
    "%cd data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Molecular system description ##"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this tutorial, we will use density fitting to fit the structure of the [G-protein coupled calcitonin receptor-like receptor (CLR)](https://en.wikipedia.org/wiki/Calcitonin_gene-related_peptide) to its cryo-EM density. The receptor has two well-defined states, the main difference between them is in the position of several transmembrane alpha-helices. We will take the density file of the active state of the receptor (EMD-20906) and fit an atomistic model of the protein into the receptor's density. \n",
    "\n",
    "Note that any type of 3D structure in PDB format can be used as an input for this procedure, ranging from homology models to experimentally derived models or outcomes from neural network-based conformation predictions. In this tutorial we will use a model generated by AlphaFold2 for the fitting. Below, you can take a look at it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Initial structure of CLR generated by AlphaFold2\n",
    "view = ng.NGLWidget()\n",
    "view.add_component(\"input/CLR_initial_model.pdb\")\n",
    "view\n",
    "#Click and drag to rotate, zoom with your mouseweel \n",
    "#For more information on this viewer have a look at https://github.com/nglviewer/nglview"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The density file `EMD-20906` was downloaded from [Electron Microscopy Data Bank](https://www.ebi.ac.uk/emdb/EMD-20906). The original file contains electron density for a big multisubunit protein complex. [Chimera](https://www.cgl.ucsf.edu/chimera/) was used to extract and save the density corresponding to CLR in a separated file. A [tutorial on density extraction using Chimera](https://youtu.be/oEtLkbApQmk?si=OpkhOs8K-zTXxSAk) is available on YouTube for further details. Below, you can see the resulting density file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "view = ng.NGLWidget()\n",
    "view.add_component('input/CLR_active_state_map_rotated.mrc')\n",
    "view"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## System preparation ##"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we prepare the system for the simulation. First of all we need to generate the structural and topology file to energy minimize the structure before performing density-fit simulation. We start by generating the topology file for the receptor using GROMACS pre-processor tool `pdb2gmx` and `CLR_initial_model.pdb` file as input. You find the file in the directory `input`. \n",
    "In this tutorial we use CHARMM36 force field. The force field is not distributed with GROMACS and be downloaded . Force field directory `charmm36-jul2022.ff` can be found in the working directory and is automatically read by the pre-processing tool.   \n",
    "\n",
    "All needed commands are listed below, for more detaild explanation of the commands please refer to GROMACS tutorial: [Introduction to Molecular Dynamics](https://tutorials.gromacs.org/md-intro-tutorial.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# prepare .gro file and topology file.\n",
    "!gmx pdb2gmx -f input/CLR_initial_model.pdb -o model.gro -water tip3p -ff \"charmm36-jul2022\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# prepare the box\n",
    "!gmx editconf -f model.gro -o model_box.gro -c -d 1.0 -bt cubic"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For tutorial purpose, the simulations are performed in water solution, even if CLR is a membrane protein. Note we can still obtain accurate results by using simulations in water ([Linnea Yvonnesdotter et al.](https://doi.org/10.1016/j.bpj.2023.05.033)). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# add water molecules\n",
    "!gmx solvate -cp model_box.gro -o model_solv.gro -p topol.top"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we add ions to neutralize the system's charge. We achieve this using `-neutral` option in `gmx genion`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#adding ions\n",
    "!touch ions.mdp\n",
    "!gmx grompp -f ions.mdp -c model_solv.gro -p topol.top -o solv.tpr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!echo 13 | gmx genion -s solv.tpr -o model_solv.gro -p topol.top -pname NA -nname CL -neutral  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After setting up the complete system, the next steps are energy minimization and equilibration. For guidance, you can refer to the [Introduction to Molecular Dynamics](https://tutorials.gromacs.org/md-intro-tutorial.html) tutorial, which wementioned earlier. You can practice system preparation using the commands from the reference tutorial or proceed directly with the provided `start.gro` file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Please remove the comment character (#) to execute the command below. \n",
    "# It copies prepared file start.gro from the input folder.\n",
    "#!cp input/start.gro ./"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "view = ng.NGLWidget()\n",
    "view.add_component('start.gro')\n",
    "view.add_representation(\"ball+stick\",selection=\"NA or CL\")\n",
    "## water\n",
    "view.add_representation(\"licorice\",selection=\"TIP3P\", opacity=0.3)\n",
    "view"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Density alignment"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To run a density-guided simulation using GROMACS we need not only a structural file (`model_solv.gro`), a topology file (`topol.top`) describing the molecular system, but also a file containing information on the density to map (`CLR_active_state_map_rotated.mrc`) and a simulation parameter file (`df.mdp`). You can find the two files in the directory `input`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we examine the density, we will notice that it is not aligned with the protein structure in `start.gro`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "view = ng.NGLWidget()\n",
    "view.add_component(\"start.gro\")\n",
    "view.add_component('input/CLR_active_state_map_rotated.mrc')\n",
    "view"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To align the density and the structure, we use the rigidbodyfit script. Here, there are two important parameters to consider:\n",
    "\n",
    "1. sampling-depth: Increasing this value will result in a more rigorous alignment. Practise indicates `9` as a reasonable values. However, if you're dissatisfied with the alignment result, consider increasing it to 11-12. Be aware that higher values may significantly extend the calculation time. \n",
    "\n",
    "2. output-transform: This option adds a transformation matrix to the output. We will use it in our md parameter file `df.mdp`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# script works with .pdb file format, so we have to convert prepared .gro to .pdb\n",
    "! gmx editconf -f start.gro -o start.pdb"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The rigidbodyfit script will run much faster locally\n",
    "! rigidbodyfit --structure start.pdb --density input/CLR_active_state_map_rotated.mrc --sampling-depth 9 --output-transform\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To check if the alignmnt was successfull we check how fitted.pdb looks compared to the density. The process is stochastic, so if you see a poorly aligned result, increase the `--sampling-depth` value and rerun the cell above.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "view = ng.NGLWidget()\n",
    "view.add_component(\"fitted.pdb\")\n",
    "view.add_component('input/CLR_active_state_map_rotated.mrc')\n",
    "view"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Most parts of the structure are aligned. Note that some residues (195-203) are slightly shifted compared to the density, so density fitting simulation is needed. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we import the information from `transform.json` to the df.mdp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cp input/df.mdp ."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mdp_shift = 'density-guided-simulation-shift-vector                  = '\n",
    "mdp_rotate = 'density-guided-simulation-transformation-matrix         = '\n",
    "mdp_name = '\\ndensity-guided-simulation-reference-density-filename    = CLR_active_state_map_rotated.mrc\\n'\n",
    "\n",
    "with open('transform.json') as f:\n",
    "    d = json.load(f)\n",
    "    a = [str(i) for i in d['shift_in_nm']]\n",
    "    out_str = mdp_shift + ' '.join(a) + '\\n'\n",
    "\n",
    "    rot = ''\n",
    "    for el in d['orientation_matrix']:\n",
    "        s = [str(i) for i in el]\n",
    "        rot = rot + ' ' + ' '.join(s)\n",
    "\n",
    "with open('df.mdp', 'a') as f2:\n",
    "    f2.write(mdp_name)\n",
    "    f2.write(out_str)\n",
    "    f2.write(mdp_rotate+rot + '\\n')       \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat df.mdp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "More information on the mdp file option for density fit simulations can be found in [GROMACS user guide](https://manual.gromacs.org/documentation/current/user-guide/mdp-options.html#density-guided-simulations), in the [BioExcel webinar](https://bioexcel.eu/webinar-density-guided-simulations-combining-cryo-em-data-and-molecular-dynamics-simulation-2020-04-28/) given by Christian Blau. Some math behind the method is reported in [GROMACS manual](https://manual.gromacs.org/current/reference-manual/special/density-guided-simulation.html).\n",
    "\n",
    "We check that the density reference file is the working directory. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If it is not, we copy the reference file in the working directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cp input/CLR_active_state_map_rotated.mrc ./"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Density guided simulation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we are ready to run the simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gmx grompp -f df.mdp -c start.gro -p topol.top -o df.tpr"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check for warnings. If you want for time reason, you can skip the simulation and copy the output files from the directory reference. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Please remove the comment character (#) to execute the command below. \n",
    "# It takes long to get the result. You can stop the kernel and use the pre-calculated simulation.\n",
    "#!gmx mdrun -s df.tpr -deffnm df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " Note: if you do not want to wait, but go on with the tutorial, copy the data from the `reference` directory into the output_files directory. To do this within this notebook, remove the comment characters (#) in the following cell"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## ONLY execute the lines below if you do not want to run and wait for the simulations to finish. \n",
    "## remove the comment characters (#) to execute the commands below\n",
    "#! cp reference/df.xtc .\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simulation analysis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, let's examine the simulation results. Since the output trajectory is not pre-aligned to the density (the transformation matrix in the parameter file accounts for this during the simulation), we now need to align it explicitly. Gromacs trjconv command with \"-fit rot+trans\" option. We align the trajectory to the `fitted.pdb` file that we generated using `rigidbodyfit`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! echo 1 0 | gmx trjconv -f df.xtc -s fitted.pdb -fit rot+trans -o df_fitted.xtc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Basic manipulations with trajectories can be done using the [MDAanalysis python library](https://doi.org/10.1002/jcc.21787). We load trajectory information from TPR and XTC files. Notice how residues 198-203 shift into the density (use the slider to navigate through the frames more quickly). At which frame do the residues appear well-aligned?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# read trajectory\n",
    "u = mda.Universe('start.gro', 'df_fitted.xtc')\n",
    "view = ng.NGLWidget()\n",
    "view.add_trajectory(u)\n",
    "view.add_component('input/CLR_active_state_map_rotated.mrc')\n",
    "view"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To visualize the progress of the simulation, we can plot RMSD (root mean square deviation of atomic positions). We calculate the RMSD of each frame compared to the input structure. In MDAnalysis, this can be done using the align.AlignTraj class. For more details, please refer to the [MDAnalysis documentation](https://docs.mdanalysis.org/2.7.0/documentation_pages/analysis/rms.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# align trajectory to the first frame and calculate RMSD. RMSD values are stored in prealigner.results.rmsd\n",
    "prealigner = align.AlignTraj(u, u, select=\"protein and name CA\",\n",
    "                             in_memory=True).run()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create list with time in ps\n",
    "time = [num*2 for num, el in enumerate(prealigner.results.rmsd)]\n",
    "\n",
    "# plot RMSD\n",
    "%matplotlib inline\n",
    "%config InlineBackend.figure_format='retina'\n",
    "plt.rcParams[\"figure.figsize\"] = (6, 4)\n",
    "plt.rcParams[\"font.size\"] = 14\n",
    "plt.plot(time, prealigner.results.rmsd)\n",
    "plt.xlabel('Time, ps')\n",
    "plt.ylabel('RMSD (C-alpha), Å')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the purposes of the tutorial we used a system for which structure has already been resolved (PDBid: [6UVA](https://www.rcsb.org/structure/6UVA)). We can assess the accuracy of our simulation by comparing the RMSD between the resolved structure and our simulation frames."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# load reference structure and pick only backbone\n",
    "reference_state = mda.Universe('input/CLR_reference.pdb') \n",
    "ref_bb = reference_state.select_atoms('name CA')\n",
    "ref_bb_positions = ref_bb.positions\n",
    "\n",
    "# Select tha same atoms that are available in the reference structure \n",
    "bb = u.select_atoms('name CA and (resid 1:193 or resid 199:224 or resid 232:272)')\n",
    "bb.write('selected_atoms_traj.pdb', frames='all')\n",
    "\n",
    "part_atoms = mda.Universe('selected_atoms_traj.pdb')\n",
    "\n",
    "# Align and calculate RMSD\n",
    "reference_alignment = align.AlignTraj(part_atoms,  # trajectory to align\n",
    "                ref_bb,  # reference\n",
    "                select='all',  # selection of atoms to align\n",
    "                filename='aligned_traj.pdb',  # file to write the trajectory to\n",
    "                match_atoms=True,  # whether to match atoms based on mass\n",
    "               ).run()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(time, reference_alignment.results.rmsd)\n",
    "plt.rcParams[\"figure.figsize\"] = (6, 4)\n",
    "plt.title('Comparison to the reference structure')\n",
    "plt.xlabel('Time, ps')\n",
    "plt.ylabel('RMSD (C-alpha), Å')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To assess the fitting quality without reference structures, we are plotting a metric such as cross-correlation, which represents the structure's similarity to the density.  The closer the value is to 1, the better the atoms fit into the density. Cross-correlation can be computed using [Chimera](https://www.cgl.ucsf.edu/chimera/) fitmap function (аn example script `cross_correlation_with_chimera.py` can be found in the `data/additional_scripts` folder). \n",
    "\n",
    "Due to the adaptive force scaling scheme the force to the protein atoms becomes extremely high towards the end of the simulations, resulting in a crash. Therefore, to prevent distortion of the protein structure and integritiy of the stereochemistry in the final mode, from this high force, we will use a generalized orientation-dependent all-atom potential [GOAP](https://doi.org/10.1016/j.bpj.2011.09.012) scoring function; originally developed to assess protein structure prediction.\n",
    "\n",
    "GOAP score source code was downloaded at [https: //sites.gatech.edu/cssb/goap/](https://sites.gatech.edu/cssb/goap/)\n",
    "\n",
    "Operating these tools falls beyond the scope of this tutorial. However, we have precalculated the cross-correlation (`cc.csv`) and GOAP-score (`goap.csv`) values for our density simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "goap = pd.read_csv('reference/goap.csv', index_col=0)['goap_score'].to_list()\n",
    "cc = pd.read_csv('reference/cc.csv', index_col=0)['cross-correlation'].to_list()\n",
    "\n",
    "plt.rcParams[\"figure.figsize\"] = (6, 4)\n",
    "fig, ax1 = plt.subplots()\n",
    "\n",
    "color = 'tab:red'\n",
    "ax1.set_xlabel('Time, ps')\n",
    "ax1.set_ylabel('Cross-correlation', color=color)\n",
    "ax1.plot(time[1:], cc[1:], color=color)\n",
    "ax1.tick_params(axis='y', labelcolor=color)\n",
    "\n",
    "fig.tight_layout()\n",
    "#plt.xlim(0, 45)\n",
    "#ax1.set_title('Cross-correlation')\n",
    "plt.subplots_adjust(top=0.9)\n",
    "\n",
    "plt.show() "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "averages = []\n",
    "window_size = 20\n",
    "for i in range(len(goap) - window_size + 1):\n",
    "    window = goap[i:i + window_size]\n",
    "    window_average = sum(window) / window_size\n",
    "    averages.append(window_average)\n",
    "\n",
    "plt.rcParams[\"figure.figsize\"] = (6, 4)\n",
    "fig, ax1 = plt.subplots()\n",
    "\n",
    "ax2 = ax1  # instantiate a second axes that shares the same x-axis\n",
    "\n",
    "color = 'tab:blue'\n",
    "ax2.set_ylabel('GOAP score', color=color)\n",
    "ax2.plot(time[1:], goap[1:], color=color)\n",
    "ax2.plot(time[:-(window_size)], averages[1:], color='indigo')\n",
    "ax2.tick_params(axis='y', labelcolor=color)\n",
    "\n",
    "fig.tight_layout()\n",
    "#plt.xlim(0, 45)\n",
    "#ax1.set_title('GOAP-score')\n",
    "plt.subplots_adjust(top=0.9)\n",
    "\n",
    "plt.show() "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The plot above shows the increase in cross-correlation over time. The GOAP-score remains fairly steady during the first half of the simulation but begins to rise after 600 ps."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have performed the density-fit simulation, we should report or recall about what type of simulation we have per formed. \n",
    "The GROMACS tool [`gmx report-methods`](https://manual.gromacs.org/current/onlinehelp/gmx-report-methods.html) can be useful for this purpose. gmx report-methods print out basic system information on a performed run. It can also provide an unformatte\n",
    "d text (with the option -o) or a LaTeX formatted output file with the option -m."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For an additional challenge, try setting up density fitting for a more complex system. You can find one in the `data/input_ion_channel` directory, that contains experimental electron density maps from the ABC transporter TmrAB (doi:10.1038/s41586-019-1391-0). Is the density aligned to the starting structure? Which regions need the most fitting?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Do you have any questions? Have a look at the user discussions on [GROMACS forums](htttp://forums.gromacs.org)"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
